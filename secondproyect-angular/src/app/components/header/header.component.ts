import { stripSummaryForJitNameSuffix } from '@angular/compiler/src/aot/util';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public logo: string;
  public chat: string;
  public videoconsulta: string;
  public  especialidades: string;
  public medicosycentros: string;
  public descubre: string;

  constructor() {
    this.logo = 'https://www.flaticon.es/svg/static/icons/svg/3893/3893729.svg';
    this.chat = 'Chat Médico';
    this.videoconsulta = 'Videoconsulta';
    this.especialidades = 'Especialidades';
    this.medicosycentros = 'Médicos y centros';
    this.descubre = 'Descubre más';
  }

  ngOnInit(): void {
  }
}
