import { Form } from '../landing/models/ILanding';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { IntroInfo, Goals} from '../landing/models/ILanding';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  public resquest: Form | null = null;
  public introinfo: IntroInfo;
  public goals: Goals;

  constructor() {
    this.introinfo = {
      title: 'WELCOME TO DIGITAL HEALTH!',
      img: [
        {
        url: '../../assets/Img/image1.jpg',
        alt: 'Health Premium Silver',
      },
      {
        url: '../../assets/Img/image2.jpg',
        alt: 'Health Premium Gold',
      },
      {
        url: '../../assets/Img/image3.jpg',
        alt: 'Health Premium Gold',
      }
    ]
    };
    this.goals = {
      title: 'La atención médica que quieres, siempre contigo',
      parragraph: 'Online y presencial. Sin póliza. Sin ataduras',
      copy: 'Prueba Nuestro Paquete Iniciación Premium GRATIS durante 1 MES',
    };
  }

  ngOnInit(): void {

  }

  // tslint:disable-next-line: typedef
  public subscribeForm(form: Form) {
    // tslint:disable-next-line: no-debugge
    this.resquest = form;
  }


}
