export interface IntroInfo {
    title: string;
    img: Img[];
}

export interface Goals {
    title: string;
    parragraph: string;
    copy: string;
}

export interface Form {
    name: string;
    mail: string;
    age: number;

}

export interface Img {
    url: string;
    alt: string;
}
