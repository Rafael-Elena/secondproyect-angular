import { Goals } from './../models/ILanding';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {
  @Input() public goals!: Goals;

  constructor() { }

  ngOnInit(): void {
  }

}
