import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Form } from '../models/ILanding';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public newsletterForm: FormGroup | null = null;
  public submitted: boolean | true = true;
  @Output() public newsletterEmiter = new EventEmitter <Form>();

  constructor(private formBuilder: FormBuilder) {
    this.newsletterForm = this.formBuilder.group ({
      name: ['', [Validators.required, Validators.minLength(2)]],
      mail: ['', [Validators.required, Validators.email]],
      age: ['', [Validators.required, Validators.min(15)]],
    });
  }

  ngOnInit(): void {
  }
  public onSubmit(): void {
    this.submitted = true;
    if (this.newsletterForm?.valid) {
      const newsletterRegister: Form = {
        name: this.newsletterForm.get('name')?.value,
        mail: this.newsletterForm.get('mail')?.value,
        age: this.newsletterForm.get('age')?.value,
      };
      this.sendForm(newsletterRegister);
      console.log(newsletterRegister);
      this.newsletterForm.reset();
    }
  }
  // tslint:disable-next-line: typedef
  public sendForm(newsletterRegister: Form) {
    this.newsletterEmiter.emit(newsletterRegister);
  }
}
